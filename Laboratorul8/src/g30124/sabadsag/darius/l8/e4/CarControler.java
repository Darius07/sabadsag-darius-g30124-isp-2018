package g30124.sabadsag.darius.l8.e4;

import java.io.*;

public class CarControler {

    Car createCar(String model,int price) {
        Car car=new Car(model, price);
        return car;
    }
    void addCar(Car car,String store) throws IOException
    {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(store))) {
            o.writeObject(car);
        }
        System.out.println("A fost scris in fisier");
    }
    Car removeCar(String store) throws IOException,ClassNotFoundException
    {
        ObjectInputStream c=new ObjectInputStream(new FileInputStream(store));
        Car car=(Car)c.readObject();
        System.out.println("A fost citit urmatorul model: ");
        return car;
    }

}
