package g30124.sabadsag.darius.l8.e4;

public class Main {
    public static void main(String[] args) throws Exception{
    CarControler c=new CarControler();
    Car car1=c.createCar("Mercedes", 70000);
    Car car2=c.createCar("Ford", 16500);
    Car car3 = c.createCar("Fiat", 9200);


    c.addCar(car1, "car1.txt");
    c.addCar(car2, "car2.txt");
    c.addCar(car3, "car3.txt");
    c.removeCar("car1.txt");
    System.out.println(car3.toString());
}
}
