package g30124.sabadsag.darius.l8.e2;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Reader {public static void main(String[] args) throws IOException {
    System.out.println("Insert the letter :");
    char l=(char) System.in.read();
    File file=new File("date.txt");
    BufferedReader bf=new BufferedReader(new FileReader(file));
    String line;
    int appearances=0;
    while((line=bf.readLine())!=null)
    {
        int line_counter=line.length();
        for(int i=0;i<line_counter;i++)
            if(line.charAt(i)==l)
                appearances++;
    }
    System.out.println("There are "+appearances+" appearances with "+l);

}
}