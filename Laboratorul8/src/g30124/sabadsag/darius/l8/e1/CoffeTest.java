package g30124.sabadsag.darius.l8.e1;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++) {
            try {
                Cofee c = mk.makeCofee();
                try {
                    d.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
                } finally {
                    System.out.println("Throw the cofee cup.\n");
                }
            }
          catch(OverflowCoffeeException nr){
            System.out.println("Exception:" + nr.getMessage() + " Number=" + nr.getNr());
        }
    }
}}
