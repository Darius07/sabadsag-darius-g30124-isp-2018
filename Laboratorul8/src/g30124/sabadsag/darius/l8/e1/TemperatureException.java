package g30124.sabadsag.darius.l8.e1;

public class TemperatureException extends Exception {
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }

}
