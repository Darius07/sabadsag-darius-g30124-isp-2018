package g30124.sabadsag.darius.l8.e1;
class OverflowCoffeeException extends Exception {
    int nr;
    public OverflowCoffeeException(int nr,String msg) {
        super(msg);
        this.nr = nr;
    }

    public int getNr() {
        return nr;
    }
}
