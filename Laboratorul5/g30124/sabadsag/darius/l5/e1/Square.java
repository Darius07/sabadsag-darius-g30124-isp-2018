package g30124.sabadsag.darius.l5.e1;

public class Square extends Rectangle {
    public Square() {
        super();
    }
    public Square(double side){
        super(side);
    }
    public Square(double side,String color,boolean filled)
    {
        super(color,filled,side,side);

    }
    public double getSide()
    {
        return super.getLength();
    }


    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public String toString() {
        return "A Square with side="+getSide()+" ,which is a sub" +
                "class of "+super.toString();
    }
}