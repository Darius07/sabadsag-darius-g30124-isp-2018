package g30124.sabadsag.darius.l5.e1;

import java.awt.geom.Area;

public abstract class Shape {
    private String color;
    private boolean filled;
    public Shape(){
    }
    public Shape(String color,Boolean filled){
        this.color=color;
        this.filled=filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public boolean isFilled(){
        return filled;
    }
    public abstract double getArea();
    public abstract double getPerimeter();

    @Override
    public String toString() {
        return "Shape{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                '}';
    }
}