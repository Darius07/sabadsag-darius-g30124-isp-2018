package g30124.sabadsag.darius.l5.e1;

public class TestShapes {
    public static void main(String[] args) {


        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(2);
        shapes[1] = new Rectangle(1, 5);
        shapes[2] = new Square(3);

        for (int i = 0; i<shapes.length; i++)

            System.out.println( shapes[i].getArea()+"\n"+shapes[i].getPerimeter());
    }
}

