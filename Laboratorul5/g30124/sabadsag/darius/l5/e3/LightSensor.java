package g30124.sabadsag.darius.l5.e3;

import java.util.Random;

public class LightSensor extends Sensor{
    public  int lightSensor;
    @Override
    public int readValue() {
        Random number=new Random();
        lightSensor=number.nextInt(100);
        return lightSensor;
    }
}
