package g30124.sabadsag.darius.l5.e4;

import g30124.sabadsag.darius.l5.e3.LightSensor;
import g30124.sabadsag.darius.l5.e3.TemperatureSensor;

import java.util.Timer;
import java.util.TimerTask;

public class Singleton {
        private static volatile Singleton instance=null;
        private Singleton() {}
        public static Singleton getInstance() {
            synchronized (Singleton.class)
            {
                if(instance==null)
                    instance=new Singleton();
            } return instance;
        }
        Timer timer=new Timer();
        LightSensor lightSensor=new LightSensor();
        TemperatureSensor temperatureSensor=new TemperatureSensor();
        private int secondsPassed=0;

        TimerTask task=new TimerTask() {
            @Override
            public void run() {
                secondsPassed++;
                if(secondsPassed>=20)
                    timer.cancel();
                System.out.println(secondsPassed + ")" + "\n" +
                        "Temperature: " + temperatureSensor.readValue() + " Light: " + lightSensor.readValue());

            }
        };
        public void Control() {

            timer.scheduleAtFixedRate(task, 1000, 1000);
            {

            }
        }

        public static void main(String[] args) {
            Singleton controller= Singleton.getInstance();
            controller.Control();
        }
    }


