package g30124.sabadsag.darius.l9.e2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Buton extends JFrame {
    JButton button;
    JTextArea textArea;
    String text= "0";
    Buton(){
        setTitle("Numara click-uri");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        button=new JButton("Click");
        button.setBounds(100,100,150,20);
        textArea=new JTextArea();
        textArea.setBounds(130,130,40,40);
        textArea.setText(text);
        add(button);
        add(textArea);
        setSize(500,500);
        setVisible(true);
        button.addActionListener(new ApasareButon());
    }

    class ApasareButon implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            String counter=Buton.this.textArea.getText();
            int c=Integer.parseInt(counter);
            c++;
            counter=Integer.toString(c);
            Buton.this.textArea.setText(counter);


        }
    }
    public static void main(String[] args) {
        new Buton();
    }
}