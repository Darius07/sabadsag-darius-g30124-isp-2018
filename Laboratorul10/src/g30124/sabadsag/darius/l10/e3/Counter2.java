package g30124.sabadsag.darius.l10.e3;

public class Counter2 extends Thread {
    Counter2(String name){
        super(name);
    }
    public void run(){
        for(int i=100;i<=200;i++) {
            System.out.println(getName() + "i=" + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(getName()+"job finished.");
    }
}
