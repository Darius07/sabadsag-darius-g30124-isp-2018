package g30124.sabadsag.darius.l10.e3;

    public class Counter extends Thread {

        Counter(String name){
            super(name);
        }

        public void run(){
            for(int i=0;i<100;i++){
                System.out.println(getName() + " i = "+i);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
        }


        public static void main(String[] args) {
            Counter c1 = new Counter("counter1");
            Counter2 c2 = new Counter2("counter2");

            c1.start();
            try {
                c1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            c2.start();
        }
    }
