package g30124.sabadsag.darius.l6.e4;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
    public class TestCharSequence {
        private CharSequence charSequence;
        @Before
        public void set()
        {  char[] sir1;
            sir1="abc";
            charSequence=new CharSequence(sir1);
        }
        @Test
        public void shouldGetChar()
        {
            assertEquals(charSequence.charAt(2),"c");
        }
    }

