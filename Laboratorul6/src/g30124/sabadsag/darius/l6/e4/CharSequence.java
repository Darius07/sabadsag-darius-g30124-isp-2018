package g30124.sabadsag.darius.l6.e4;



public class CharSequence implements java.lang.CharSequence {
    private char [] ch;

    public CharSequence(char[] ch) {
        this.ch = ch;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return ch[index];
    }

    @Override
    public java.lang.CharSequence subSequence(int start, int end) {
        return null;
    }
}
