package g30124.sabadsag.darius.l6.e1;

import java.awt.*;

public class Rectangle extends Shape {

    private int length;

    public Rectangle(Color color, int length) {
        super(color);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel " + length + " " + getColor().toString());
        g.setColor(getColor());
        g.drawRect(super.getX(),super.getY(), length, length);
    if(super.isFilled()==true)
        g.fillRect(super.getX(),super.getY(),length,length);
    }


}
