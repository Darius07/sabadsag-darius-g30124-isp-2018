package g30124.sabadsag.darius.l6.e3;

import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,120,95,"1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,40,60,"2",false);
        b1.addShape(s2);
        Shape s3 = new Circle(Color.ORANGE, 75,70,30,"3",true);
        b1.addShape(s3);
    }
}
