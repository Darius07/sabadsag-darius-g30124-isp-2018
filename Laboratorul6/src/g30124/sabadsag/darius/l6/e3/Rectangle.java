package g30124.sabadsag.darius.l6.e3;

import java.awt.*;

public class Rectangle implements Shape{
private int length,x,y;
private String id;
private boolean isFilled;
private Color color;

public Rectangle(Color color, int length, int x, int y, String id, boolean isFilled) {
        this.color=color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.isFilled=isFilled;
        this.length = length;
        }

    public Color getColor() {
        return color;
    }

    @Override
    public String getId() {
        return id;
}

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.drawRect(x,y,length,length);
        if(isFilled==true)
            g.fillRect(x,y,length,length);
    }



}
