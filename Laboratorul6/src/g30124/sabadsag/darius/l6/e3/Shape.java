package g30124.sabadsag.darius.l6.e3;

import java.awt.*;

    public  interface  Shape {
        void draw(Graphics g);
        String getId();
    }

