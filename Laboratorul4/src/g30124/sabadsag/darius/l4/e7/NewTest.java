package g30124.sabadsag.darius.l4.e7;

import org.junit.Before;
import org.junit.Test;

public class NewTest {
    private Circle circle;
    private Cylinder cylinder;
    private Cylinder cylinder1;
    @Before
    public void setUp()
    {
        cylinder=new Cylinder(4,4);
        circle=new Circle(2);
        cylinder1=new Cylinder();
    }
    @Test
    public void TestGetVolume()
    {
        assertEquals(cylinder.getVolume(),78.52);
    }

    private void assertEquals(double radius, double v) {
    }

    @Test
    public void TestGetRadius()
    {
        assertEquals(circle.getRadius(),2);
        assertEquals(cylinder1.getRadius(),6); // implicit constructor
    }

    @Test
    public void TestGetArea()
    {
        assertEquals(circle.getArea(),35.6);
        assertEquals(cylinder.getArea(),103.9);
    }
    @Test
    public void TestGetHeight()
    {
        assertEquals(cylinder.getHeight(),4);
    }
}
