package g30124.sabadsag.darius.l4.e7;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        super();
        height = 1.0;
    }

    public Cylinder(double height) {
        this.height = height;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea() {// TotalArea=2*pi*r(Radius+Height)
        return 2 * super.getArea() + 2 * Math.PI * getRadius() * height;
    }

    public double getVolume() {
        return super.getArea() * height;
    }
}