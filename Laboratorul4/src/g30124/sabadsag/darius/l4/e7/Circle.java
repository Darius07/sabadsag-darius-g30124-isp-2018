package g30124.sabadsag.darius.l4.e7;


import static java.lang.Math.round;

public class Circle {private double radius=2;
    private String color="red";
    public Circle() {
        radius=2;
        color="red";
    }

    public Circle(double radius) {
        this.radius=radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea() {
        return (Math.PI*Math.pow(radius,2));
    }
 
    public String toString() {
        return "Circle radius "+ radius + ", color=" + color ;
    }
}
