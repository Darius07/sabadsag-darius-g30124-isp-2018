package g30124.sabadsag.darius.l4.e2;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
public class TestMyPoint {
    private MyPoint point;
    @Before
    public void setUp()
    {
        point=new MyPoint();
    }

    @Test
    public void setXTest()
    {
        point.setX(5);
        assertEquals(point.getX(),5);
    }
    @Test
    public void setYTest()
    {
        point.setY(20);
        assertEquals(point.getY(),20);
    }
    @Test
    public void setXYTest()
    {
        point.setXY(4,9);
        assertEquals(point.getX(),4);
        assertEquals(point.getY(),9);
    }
    @Test
    public void testDistance(){
        point.setXY(4,5);

        assertEquals(point.distance(2,5),2.0);
    }
    @Test
    public void testDistancePoint()
    {
        point.setXY(4,6);
        MyPoint point2=new MyPoint(0,6);
        assertEquals(point.distance(point2),4.0);
    }
}
