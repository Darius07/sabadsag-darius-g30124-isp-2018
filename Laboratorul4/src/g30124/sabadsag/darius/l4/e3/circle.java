package g30124.sabadsag.darius.l4.e3;

public class circle {
    private double radius=1.0;
    private String color="red";
    public circle ( double r) {
        radius=r;
    }
    public circle(){
        radius=1.0;
    }
    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
    }
}
