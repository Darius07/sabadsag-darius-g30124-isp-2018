package g30124.sabadsag.darius.l4.e3;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CircleTest {
    private circle c;
    @Before
 public  void setUp  () {
        c = new circle(2);
    }
    @Test
    public void getRadius(){
        assertEquals(c.getRadius(),2,2);
    }
    @Test
    public void shouldGetArea() {
        assertEquals(c.getArea(),12.56,2);
    }
}
