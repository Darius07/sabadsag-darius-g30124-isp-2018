package g30124.sabadsag.darius.l4.e5;

import g30124.sabadsag.darius.l4.e4.Author;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class BookTest {
    private Book book;

    @Before
    public void setUp() {
        Author author = new Author("Darius", "darius.sabad@yahoo.com", 'm');
        book=new Book("Life", author,6.7);
    }
    @Test
    public void testName() {
        assertEquals(book.getName(),"Life");
    }

    @Test
    public void getQty() {
        book.setQtyInStock(10);
        assertEquals(book.getQtyInStock(),10);

    }

    @Test
    public void TestPrintInfo() {
        assertEquals("Book name=Life Author Darius (m) at darius.sabad@yahoo.com",book.toString());
    }
}
