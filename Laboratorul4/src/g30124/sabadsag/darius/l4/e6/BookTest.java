package g30124.sabadsag.darius.l4.e6;

import g30124.sabadsag.darius.l4.e4.Author;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class BookTest {
    private Author aut1;
    private Author aut2;
    private Author[] aut;
    private Book noua;
    @Before
    public void setUp() {
        aut1=new Author("Sabadsag Darius","darius.sabad",'m');
        aut2=new Author("Valcica Adela","Adela.V@gmail.com",'f');
        aut=new Author[]{aut1 , aut2};
        noua=new Book ("Pacala",aut,11.4);
    }
    @Test
    public void TestGetPrice() {
        assertEquals(noua.getPrice(),11.4);
        noua.setPrice(76.4);
        assertEquals(noua.getPrice(),76.4);
    }
    @Test
    public void TestPrintAuthors(){
        noua.printAuthors();
    }
}
