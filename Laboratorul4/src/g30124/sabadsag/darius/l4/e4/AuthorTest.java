package g30124.sabadsag.darius.l4.e4;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class AuthorTest {private Author author;
    @Before
    public void setUp(){
        author=new Author("Sabadsag Darius","darius.sabad@gmail.com",'m');

    }
    @Test
    public void setEmailTest()
    {
        author.setEmail("pop_mihai@gmail.com.com");
        assertEquals(author.getEmail(),"pop_mihai@gmail.com.com");
    }
    @Test
    public void getGenderTest()
    {
        assertEquals(author.getGender(),'m');
    }
    @Test
    public void getNameTest()
    {
        assertEquals(author.getName(),"Sabadsag Darius");
    }
    @Test
    public void stringMethod()
    {
        Author author1=new Author("Cristi","stoiancristian@gmail.com",'m');
        assertEquals(author1.toString(),"Author Cristi (m) at stoiancristian@gmail.com");
    }
}
