package g30124.sabadsag.darius.l3;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestMyPoint {
    private MyPoint point;
    @Before
    public void setUp()
    {
        point=new MyPoint();
    }

    @Test
    public void setXTest()
    {
        point.setX(5);
        assertEquals(point.getX(),5);
    }
    @Test
    public void setYTest()
    {
        point.setY(20);
        assertEquals(point.getY(),20);
    }
    @Test
    public void setXYTest()
    {
        point.setXY(4,9);
        assertEquals(point.getX(),4);
        assertEquals(point.getY(),9);
    }
}
