package g30124.sabadsag.darius.l3;

public class MyPoint {
    private int x;
    private int y;
    public MyPoint()
    {
        x=0;
        y=0;
    }
    public MyPoint(int x1,int y1)
    {
        x=x1;
        y=y1;
    }
    public void setX(int x1)
    {

        x=x1;
    }
    public void setY(int y1)
    {
        y=y1;
    }
    public int getX() {

        return x;
    }

    public int getY() {

        return y;
    }

    public void setXY(int x1,int y1)
    {
        x=x1;
        y=y1;
    }
    public String toString()
    {

        return "("+getX()+","+getY()+")";
    }
    public double distance(int x1, int y1)
    {
        return Math.sqrt( Math.pow(x1-x,2)+Math.pow(y1-y,2) );
    }

    public double distance (MyPoint point2)
    {
        return Math.sqrt(Math.pow(point2.getX()-x,2)+Math.pow(point2.getY()-y,2));

    }
}
