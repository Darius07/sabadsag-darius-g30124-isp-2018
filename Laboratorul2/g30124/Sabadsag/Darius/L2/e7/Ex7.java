package g30124.Sabadsag.Darius.L2.e7;

import java.util.Random;
import java.util.Scanner;

public class Ex7 {
    public static void main(String[] args) {
        int generatedNumber,guessNumber;
        Scanner input=new Scanner(System.in);
        Random nr=new Random();
        generatedNumber=nr.nextInt(20);
        int i,guess=0;
        for(i=1;i<=3 && guess==0;i++)
        {
            System.out.println("Try to guess: ");
            guessNumber=input.nextInt();
            if(generatedNumber==guessNumber) {
                guess = 1;
                System.out.println("Congratulations!");

            }
            else
            if(generatedNumber<guessNumber)
                System.out.println("Your number is too high\n");
            else
                System.out.println("Your number is too low \n");

        }
        if(guess==0)
            System.out.println("You lost\n The number was : "+generatedNumber);
    }
}

