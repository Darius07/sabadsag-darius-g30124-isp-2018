package g30124.Sabadsag.Darius.L2.e4;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int number;
        System.out.println("Enter the number of elementes: ");
        number=input.nextInt();
        int Vector []=new int[number];
        readVector(Vector,number);
        showMaxElement(Vector,number);
    }
    static void readVector ( int V[],int n)
    {Scanner in=new Scanner(System.in);
        int i;
        for(i=0;i<n;i++) {
            System.out.println("Enter the "+i+" element");
            V[i]=in.nextInt();
        }

    }
    static void showMaxElement(int V[],int n)
    {
        int max=0,i;
        for(i=0;i<n;i++)
            if( V[i]>max)
                max=V[i];
        System.out.println("The maximum element is : "+ max);
    }
}
