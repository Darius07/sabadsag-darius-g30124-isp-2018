package g30124.Sabadsag.Darius.L2.e5;

import java.util.Random;

public class Ex5 {
    public static void main(String[] args) {
        int Vector []=new int[10];
        GenerateArray(Vector);
        sortArray(Vector);

    }
    static void GenerateArray(int V[])
    {
        Random ran=new Random();
        int i;
        for(i=0;i<9;i++)
            V[i]=ran.nextInt(50);

    }
    static void sortArray(int V[])
    {
        int i,j,aux;

        for(i=0;i<9;i++)
            for(j=0;j<9-i-1;j++)
                if(V[j]>V[j+1])
                {
                    aux=V[j];
                    V[j]=V[j+1];
                    V[j+1]=aux;
                }
        System.out.println("\n ");
        for(i=0;i<9;i++)
            System.out.println(V[i]+" ");
    }
}

