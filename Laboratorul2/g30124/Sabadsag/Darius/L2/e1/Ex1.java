package g30124.Sabadsag.Darius.L2.e1;

import java.util.Scanner;

public class Ex1 {
    public static void main (String [] args) {
        int a, b;
        Scanner input = new Scanner(System.in);
        System.out.println("a=");
        a = input.nextInt();
        System.out.println("b=");
        b = input.nextInt();
        if (a > b)
            System.out.println("The max is " + a);
        else
            System.out.println("The max is " + b);
    }
}
