package g30124.sabadsag.darius.l7.e3;

import g30124.sabadsag.darius.l7.e1.BankAccount;

import java.util.Iterator;
import java.util.TreeSet;

public class BankTreeSet {
    TreeSet<BankAccount> accounts=new TreeSet<>();
    public void addAccount(String owner,double balance)
    {
        BankAccount a=new BankAccount(owner,balance);
        accounts.add(a);
    }
    public void printAccounts()
    {
        for(BankAccount a:accounts)
            System.out.println(a.toString());
    }

    public void printAccounts(double minBalance,double maxBalance)
    {
        for(BankAccount a:accounts)
        {
            if(a.getBalance() > minBalance && a.getBalance() < maxBalance)
                System.out.println(a.toString());
        }
    }

    public BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator=accounts.iterator();
        while(iterator.hasNext())
        {
            BankAccount a= iterator.next();
            if(a.getOwner().equals(owner))
                return a;

        }
        return null;
    }

    public TreeSet<BankAccount> getAccounts() {
        return accounts;
    }
}
