package g30124.sabadsag.darius.l7.e3;


import g30124.sabadsag.darius.l7.e2.Bank;

import java.util.Iterator;
import java.util.Set;

public class Main {
    static void displayAll(Set list)
    {
        Iterator i=list.iterator();
        while(i.hasNext())
        {
            String s=(String)i.next();
            System.out.println(s);
        }
    }
    public static void main(String[] args) {
        Bank bank=new Bank();
        bank.addAccount("Cipri",123.1);
        bank.addAccount("Simi",4000);
        bank.addAccount("Serj",12.5);
        bank.printAccounts();
    }
}
