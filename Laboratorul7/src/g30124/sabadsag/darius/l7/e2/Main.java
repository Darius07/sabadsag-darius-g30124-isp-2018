package g30124.sabadsag.darius.l7.e2;



import g30124.sabadsag.darius.l7.e1.BankAccount;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
                Bank bank=new Bank();

                bank.addAccount("Cipri",123.1);
                bank.addAccount("Simi",4000);
                bank.addAccount("Serj",12.5);
                bank.printAccounts();
                System.out.println("....");
                ArrayList<BankAccount> bankAccounts= bank.getAccounts();
                bankAccounts.sort(BankAccount.BankComparator);
                for(BankAccount a:bankAccounts)
                    System.out.println(a.toString());

    }
}


