package g30124.sabadsag.darius.l7.e2;

import g30124.sabadsag.darius.l7.e1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Bank   {
    ArrayList<BankAccount> accounts=new ArrayList<>();

    public void addAccount(String owner,double balance)
    {
        BankAccount a=new BankAccount(owner,balance);
        accounts.add(a);
    }
    public void printAccounts()
    {   sortList();
        for(BankAccount a:accounts)
            System.out.println(a.toString());
    }
    public void sortList()
    {
        Collections.sort(accounts);
    }

    public void printAccounts(double minBalance,double maxBalance)
    {   sortList();
        for(BankAccount a:accounts)
        {
            if(a.getBalance() > minBalance && a.getBalance() < maxBalance)
                System.out.println(a.toString());
        }
    }

    public BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator=accounts.iterator();
        while(iterator.hasNext())
        {
            BankAccount a= iterator.next();
            if(a.getOwner().equals(owner))
                return a;

        }
        return null;
    }

    public ArrayList<BankAccount> getAccounts() {
        return accounts;
    }
}


