package g30124.sabadsag.darius.l7.e4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        Dictionary dictionary=new Dictionary();
        char choice;
        Word word;
        Definition definition;
        String line;
        BufferedReader input=new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("-MENU-");
            System.out.println("a- Add word!");
            System.out.println("c- Search word!");
            System.out.println("l-List");
            System.out.println("e-Exit");
            line=input.readLine();
            choice=line.charAt(0);
            switch (choice)
            {
                case 'a': case 'A':
                System.out.println("Add word!");
                line=input.readLine();
                if(line.length()>1)
                {  word=new Word(line);
                    line =input.readLine();
                    definition=new Definition(line);
                    dictionary.addWord(word,definition);
                } break;
                case 'c': case 'C':
                System.out.println("Search word!");
                line=input.readLine();
                if(line.length()>1)
                {
                    word=new Word(line);
                    Definition search=dictionary.getDefinition(word);
                    if(search!=null)
                        System.out.println("Definition:"+search);
                    else
                        System.out.println("Doesn't exist!");
                }break;
                case 'l': case 'L':
                System.out.println("List:");
                dictionary.getAllWords();
                break;
            }
        }while ( choice!='e' && choice!='E');
    }
}
