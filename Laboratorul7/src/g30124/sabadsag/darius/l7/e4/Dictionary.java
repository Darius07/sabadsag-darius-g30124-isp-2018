package g30124.sabadsag.darius.l7.e4;

import java.util.HashMap;

public class Dictionary { HashMap dictionary=new HashMap();
    public void addWord(Word word,Definition definition)
    {
        if(dictionary.containsKey(word))
            System.out.println("The word already exist,add another one!");
        else
            System.out.println("Add word!");
        dictionary.put(word,definition);
    }

    public Definition getDefinition(Word word)
    {
        System.out.println(dictionary.containsKey(word));
        return (Definition) dictionary.get(word);
    }

    public void getAllWords()
    {

        dictionary.forEach((key,value)-> System.out.println(key));
    }
    public void getAllDefinitions()
    {
        dictionary.forEach((key, value) -> System.out.println( value));
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "dictionary=" + dictionary +
                '}';
    }
}
