package g30124.sabadsag.darius.l7.e1;

import java.util.Comparator;
import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balanced) {
        this.owner = owner;
        this.balance = balanced;
    }

    public String getOwner() {
        return owner;
    }


    public void deposit(double amount) {
        if (amount > 0)
            balance = balance + amount;
    }

    public void windraw(double amount) {
        if (balance > 0 && balance > amount)
            balance = balance - amount;
        else {
            System.out.println("Not enought amount");
        }
    }



@Override
public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass())
        return false;
    BankAccount t = (BankAccount) o;
    return t.balance == balance && t.owner.equals(owner);
}
@Override
public int hashCode(){
    return Objects.hash(owner,balance);
}
@Override
public int compareTo(BankAccount o){
    return (int)(balance-o.balance);
}
public static Comparator<BankAccount> BankComparator = new Comparator<BankAccount>() {
    public int compare(BankAccount a1, BankAccount a2) {
        String BankName1 = a1.getOwner().toUpperCase();
        String BankName2 = a2.getOwner().toUpperCase();

        return BankName1.compareTo(BankName2);
    }
};

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
    public double getBalance() {
        return balance;
    }
}


